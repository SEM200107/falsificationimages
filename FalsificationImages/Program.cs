﻿using ExifLib;

class Program
{
    static void Main()
    {
        string imagePath = "путь к изображению";
        try
        {
            bool isPhotoshopEdited = IsPhotoshopEdited(imagePath);

            if (isPhotoshopEdited)
            {
                Console.WriteLine("Изображение было изменено в Photoshop.");
            }
            else
            {
                Console.WriteLine("Изображение не было изменено в Photoshop.");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Ошибка при чтении изображения: " + ex.Message);
        }
    }

    static bool IsPhotoshopEdited(string imagePath)
    {
        using (FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read))
        {
            try
            {
                using (ExifReader exifReader = new ExifReader(fileStream))
                {
                    string softwareTag;
                    exifReader.GetTagValue(ExifTags.Software, out softwareTag);

                    // Проверяем наличие ключевого слова "Photoshop" в поле Software метаданных
                    if (!string.IsNullOrEmpty(softwareTag) && softwareTag.IndexOf("Photoshop", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        return true;
                    }
                }
            }
            catch (ExifLibException ex)
            {
                return false;
            }
        }

        return false;
    }
}